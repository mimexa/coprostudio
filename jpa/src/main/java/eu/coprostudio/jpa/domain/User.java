package eu.coprostudio.jpa.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * The type User.
 */
@Data
@Entity
@Table(name = "USER")
public class User {

    /**
     * The Id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The Email.
     */
    @Column(name = "EMAIL")
    private String email;

}
