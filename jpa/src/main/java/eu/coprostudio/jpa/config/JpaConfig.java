package eu.coprostudio.jpa.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * The type Jpa config.
 */
@Configuration
@ComponentScan("eu.coprostudio.jpa.services")
@EntityScan({"eu.coprostudio.jpa.domain"})
@EnableJpaRepositories(basePackages = {"eu.coprostudio.jpa.repositories"})
public class JpaConfig {
}
