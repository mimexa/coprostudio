package eu.coprostudio.jpa.services;

import eu.coprostudio.jpa.domain.User;
import io.swagger.models.auth.In;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

/**
 * The interface User repository service.
 */
public interface UserRepositoryService {

    /**
     * Gets all.
     *
     * @param coproId the copro id
     * @param request the request
     * @return the all
     */
    Page<User> getAll(Long coproId, Pageable request);

    /**
     * Get user.
     *
     * @param userId the user id
     * @return the user
     */
    User get(Long userId);

    /**
     * Update user.
     *
     * @param user the user
     * @return the user
     */
    User update(User user);

    /**
     * Create user.
     *
     * @param user the user
     * @return the user
     */
    User create(User user);

    /**
     * Delete.
     *
     * @param userId the user id
     */
    void delete(Long userId);

}
