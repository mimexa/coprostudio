package eu.coprostudio.jpa.services;

import eu.coprostudio.jpa.domain.User;
import eu.coprostudio.jpa.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import eu.coprostudio.core.exceptions.NotFoundException;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * The type User repository service.
 */
@Service
@AllArgsConstructor
public class UserRepositoryServiceImpl implements UserRepositoryService {

    /**
     * The Repository.
     */
    private UserRepository repository;

    /**
     * {@inheritDoc}
     */
    @Override
    public Page<User> getAll(Long coproId, Pageable request) {
        return repository.findAll(request);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User get(Long userId) {
        Optional<User> user = repository.findById(userId);
        if (!user.isPresent()) {
            throw new NotFoundException(String.format("No user found for id [%d]", userId));
        }
        return user.get();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User update(User user) {
        get(user.getId());
        User updated = repository.save(user);
        return updated;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User create(User user) {
        User updated = repository.save(user);
        return updated;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void delete(Long userId) {
        get(userId);
        repository.deleteById(userId);
    }

}
