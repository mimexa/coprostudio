package eu.coprostudio.jpa.repositories;

import eu.coprostudio.jpa.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

/**
 * The interface User repository.
 */
public interface UserRepository extends CrudRepository<User, Long> {

    /**
     * Find all page.
     *
     * @param request the request
     * @return the page
     */
    Page<User> findAll(Pageable request);

}
