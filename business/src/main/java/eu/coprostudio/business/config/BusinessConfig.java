package eu.coprostudio.business.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import eu.coprostudio.jpa.config.JpaConfig;

/**
 * The type Business config.
 */
@Configuration
@ComponentScan("eu.coprostudio.business.services")
@Import({JpaConfig.class})
public class BusinessConfig {
}
