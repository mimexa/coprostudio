package eu.coprostudio.business.services;

import eu.coprostudio.openapi.model.UserDto;
import eu.coprostudio.openapi.model.Users;

/**
 * The interface User service.
 */
public interface UserService {

    /**
     * Gets all.
     *
     * @param coproId  the copro id
     * @param pageNb   the page nb
     * @param pageSize the page size
     * @return the all
     */
    Users getAll(Long coproId, Integer pageSize, Integer pageNb);

    /**
     * Get user.
     *
     * @param userId the user id
     * @return the user
     */
    UserDto get(Long userId);

    /**
     * Update user.
     *
     * @param user the user
     * @return the user
     */
    UserDto update(UserDto user);

    /**
     * Create user.
     *
     * @param user the user
     * @return the user
     */
    UserDto create(UserDto user);

    /**
     * Delete.
     *
     * @param userId the user id
     */
    void delete(Long userId);

}
