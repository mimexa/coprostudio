package eu.coprostudio.business.services;

import eu.coprostudio.core.utils.Utils;
import eu.coprostudio.jpa.domain.User;
import eu.coprostudio.jpa.services.UserRepositoryService;
import eu.coprostudio.openapi.model.UserDto;
import eu.coprostudio.openapi.model.Users;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 * The type User service.
 */
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    /**
     * The Service.
     */
    private UserRepositoryService service;

    /**
     * {@inheritDoc}
     */
    @Override
    public Users getAll(Long coproId, Integer pageSize, Integer pageNb) {
        Page<User> page = service.getAll(coproId, PageRequest.of(pageNb, pageSize));
        Users users = new Users();
        users.setNbPage(pageNb);
        users.setPageSize(pageSize);
        users.setTotalElements(page.getNumberOfElements());
        users.setTotalPages(page.getTotalPages());
        users.setContent(Utils.dozerMap(page.getContent(), UserDto.class));
        return users;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserDto get(Long userId) {
        User user = service.get(userId);
        return Utils.dozerMap(user, UserDto.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserDto update(UserDto userDto) {
        User user = service.update(Utils.dozerMap(userDto, User.class));
        return Utils.dozerMap(user, UserDto.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserDto create(UserDto userDto) {
        User user = service.create(Utils.dozerMap(userDto, User.class));
        return Utils.dozerMap(user, UserDto.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(Long userId) {
        service.delete(userId);
    }

}
