package eu.coprostudio.openapi.api;

import eu.coprostudio.openapi.model.Error;
import eu.coprostudio.openapi.model.UserDto;
import eu.coprostudio.openapi.model.Users;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A delegate to be called by the {@link UsersApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-07-09T08:22:13.529075+02:00[Europe/Luxembourg]")
public interface UsersApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /users : Create a specific user
     *
     * @param user The user details (required)
     * @return The user as created with ID (status code 201)
     *         or unexpected error (status code 200)
     * @see UsersApi#create
     */
    default ResponseEntity<UserDto> create(UserDto user) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"id\" : 5, \"tag\" : \"tag\", \"email\" : \"email\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * DELETE /users/{id} : Delete a specific user
     *
     * @param id The id of the user to retrieve (required)
     * @return Expected response to a valid request (status code 200)
     *         or unexpected error (status code 200)
     * @see UsersApi#delete
     */
    default ResponseEntity<Void> delete(Long id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /users/{id} : Info for a specific user
     *
     * @param id The id of the user to retrieve (required)
     * @return Expected response to a valid request (status code 200)
     *         or unexpected error (status code 200)
     * @see UsersApi#get
     */
    default ResponseEntity<UserDto> get(Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"id\" : 5, \"tag\" : \"tag\", \"email\" : \"email\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /users : List all users
     *
     * @param coproId How many items to return at one time (max 100) (required)
     * @param pageSize How many items to return at one time (max 100) (required)
     * @param pageNb Page number (required)
     * @return A paged array of users (status code 200)
     *         or unexpected error (status code 200)
     * @see UsersApi#getAll
     */
    default ResponseEntity<Users> getAll(Long coproId,
        Integer pageSize,
        Integer pageNb) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"totalPages\" : 0, \"pageSize\" : 5, \"nbPage\" : 1, \"content\" : [ { \"id\" : 5, \"tag\" : \"tag\", \"email\" : \"email\" }, { \"id\" : 5, \"tag\" : \"tag\", \"email\" : \"email\" } ], \"totalElements\" : 6 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * PUT /users : Update a specific user
     *
     * @param user The user details (required)
     * @return The user as updated (status code 200)
     *         or unexpected error (status code 200)
     * @see UsersApi#update
     */
    default ResponseEntity<UserDto> update(UserDto user) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"id\" : 5, \"tag\" : \"tag\", \"email\" : \"email\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
