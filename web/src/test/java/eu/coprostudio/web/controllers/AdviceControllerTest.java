package eu.coprostudio.web.controllers;

import eu.coprostudio.core.exceptions.NotFoundException;
import eu.coprostudio.openapi.model.Error;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Advice controller test.
 */
@SpringBootTest
public class AdviceControllerTest {

    /**
     * The Controller.
     */
    @Autowired
    private AdviceController controller;

    /**
     * Not found exception test.
     */
    @Test
    public void notFoundExceptionTest() {
        Error error = controller.notFoundException(new NotFoundException("test"));
        Assertions.assertEquals("test", error.getMessage());
    }

}
