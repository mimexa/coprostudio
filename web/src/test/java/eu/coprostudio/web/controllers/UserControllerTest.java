package eu.coprostudio.web.controllers;

import eu.coprostudio.core.exceptions.NotFoundException;
import eu.coprostudio.core.utils.Utils;
import eu.coprostudio.jpa.domain.User;
import eu.coprostudio.jpa.repositories.UserRepository;
import eu.coprostudio.openapi.model.UserDto;
import eu.coprostudio.openapi.model.Users;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

/**
 * The type User controller test.
 */
@SpringBootTest
public class UserControllerTest {

    /**
     * The Controller.
     */
    @Autowired
    private UserController controller;

    /**
     * The Repository.
     */
    @Autowired
    private UserRepository repository;

    /**
     * After.
     */
    @AfterEach
    public void after() {
        repository.deleteAll();
    }

    /**
     * After.
     */
    @BeforeEach
    public void before() {
        repository.deleteAll();
    }

    /**
     * Gets all test.
     */
    @Test
    public void getAllTest() {
        User newUser = new User();
        newUser.setEmail("chuck@norris.com");
        repository.save(newUser);
        ResponseEntity<Users> users = controller.getAll(1l, 10, 0);
        Assertions.assertNotNull(users.getBody());
        Assertions.assertEquals(0, users.getBody().getNbPage());
        Assertions.assertEquals(10, users.getBody().getPageSize());
        Assertions.assertEquals(1, users.getBody().getTotalElements());
        Assertions.assertEquals(1, users.getBody().getTotalPages());
    }

    /**
     * Gets test.
     */
    @Test
    public void getTest() {
        User newUser = new User();
        newUser.setEmail("chuck@norris.com");
        repository.save(newUser);
        User user = repository.findAll().iterator().next();
        ResponseEntity<UserDto> dto = controller.get(user.getId());
        Assertions.assertNotNull(dto.getBody());
        Assertions.assertEquals("chuck@norris.com", dto.getBody().getEmail());
        Assertions.assertEquals(user.getId(), dto.getBody().getId());
    }

    /**
     * Gets test not found exception.
     */
    @Test
    public void getTestNotFoundException() {
        Assertions.assertThrows(NotFoundException.class, () -> {
            controller.get(100l);
        });
    }

    /**
     * Update test.
     */
    @Test
    public void updateTest() {
        User newUser = new User();
        newUser.setEmail("chuck@norris.com");
        repository.save(newUser);
        User user = repository.findAll().iterator().next();
        user.setEmail("sylvester@stalone.com");
        ResponseEntity<UserDto> updated = controller.update(Utils.dozerMap(user, UserDto.class));
        Assertions.assertNotNull(updated.getBody());
        Assertions.assertEquals("sylvester@stalone.com", updated.getBody().getEmail());
    }

    /**
     * Create test.
     */
    @Test
    public void createTest() {
        UserDto newUser = new UserDto();
        newUser.setEmail("chuck@norris.com");
        ResponseEntity<UserDto> created = controller.create(newUser);
        Assertions.assertNotNull(created.getBody());
        Assertions.assertEquals("chuck@norris.com", created.getBody().getEmail());
        Assertions.assertNotNull(created.getBody().getId());
    }

    /**
     * Delete test.
     */
    @Test
    public void deleteTest() {
        User newUser = new User();
        newUser.setEmail("chuck@norris.com");
        repository.save(newUser);
        User createdUser = repository.findAll().iterator().next();
        controller.delete(createdUser.getId());
        Assertions.assertFalse(repository.findAll().iterator().hasNext());
    }

}
