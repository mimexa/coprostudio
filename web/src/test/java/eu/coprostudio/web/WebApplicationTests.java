package eu.coprostudio.web;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Web application tests.
 */
@SpringBootTest
class WebApplicationTests {

    /**
     * Context loads.
     */
    @Test
    void contextLoads() {
    }

}
