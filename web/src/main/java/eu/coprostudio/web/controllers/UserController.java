package eu.coprostudio.web.controllers;

import eu.coprostudio.business.services.UserService;
import eu.coprostudio.openapi.api.UsersApi;
import eu.coprostudio.openapi.model.UserDto;
import eu.coprostudio.openapi.model.Users;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * The type User controller.
 */
@RestController
@AllArgsConstructor
public class UserController implements UsersApi {

    /**
     * The Service.
     */
    private UserService service;

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<UserDto> create(@NotNull @Valid UserDto user) {
        return new ResponseEntity<>(service.create(user), HttpStatus.CREATED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<Void> delete(Long id) {
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<UserDto> get(Long id) {
        return new ResponseEntity<>(service.get(id), HttpStatus.OK);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<Users> getAll(
            @NotNull @Valid Long coproId,
            @NotNull @Valid Integer pageSize,
            @NotNull @Valid Integer pageNb) {
        return new ResponseEntity<>(service.getAll(coproId, pageSize, pageNb), HttpStatus.OK);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<UserDto> update(@NotNull @Valid UserDto user) {
        return new ResponseEntity<>(service.update(user), HttpStatus.OK);
    }

}
