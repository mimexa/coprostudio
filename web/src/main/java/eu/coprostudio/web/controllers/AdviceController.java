package eu.coprostudio.web.controllers;

import eu.coprostudio.core.exceptions.CoproStudioException;
import eu.coprostudio.core.exceptions.NotFoundException;
import eu.coprostudio.openapi.model.Error;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Advice controller.
 */
@RestController
@ControllerAdvice
public class AdviceController {

    /**
     * Not found exception error.
     *
     * @param e the e
     * @return the error
     */
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error notFoundException(CoproStudioException e) {
        return e.getError();
    }

}
