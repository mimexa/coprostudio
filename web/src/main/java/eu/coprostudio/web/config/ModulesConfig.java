package eu.coprostudio.web.config;

import eu.coprostudio.business.config.BusinessConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


/**
 * The type Modules config.
 */
@Configuration
@Import({BusinessConfig.class})
public class ModulesConfig {
}
