package eu.coprostudio.core.exceptions;

import eu.coprostudio.openapi.model.Error;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * The type Copro studio exception.
 */
@Data
public class CoproStudioException extends RuntimeException {

    /**
     * The Error.
     */
    private Error error;

    /**
     * Instantiates a new Copro studio exception.
     */
    public CoproStudioException() {
        super();
        error = new Error();
        error.setMessage(StringUtils.EMPTY);
    }

    /**
     * Instantiates a new Copro studio exception.
     *
     * @param message the message
     */
    public CoproStudioException(String message) {
        super(message);
        error = new Error();
        error.setMessage(message);
    }

    /**
     * Instantiates a new Copro studio exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public CoproStudioException(String message, Throwable cause) {
        super(message, cause);
        error = new Error();
        error.setMessage(message);
    }

    /**
     * Instantiates a new Copro studio exception.
     *
     * @param cause the cause
     */
    public CoproStudioException(Throwable cause) {
        super(cause);
        error = new Error();
        error.setMessage(StringUtils.EMPTY);
    }

    /**
     * Instantiates a new Copro studio exception.
     *
     * @param message            the message
     * @param cause              the cause
     * @param enableSuppression  the enable suppression
     * @param writableStackTrace the writable stack trace
     */
    protected CoproStudioException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        error = new Error();
        error.setMessage(message);
    }

}
