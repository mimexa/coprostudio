package eu.coprostudio.core.exceptions;

/**
 * The type Not found exception.
 */
public class NotFoundException extends CoproStudioException {

    /**
     * Instantiates a new Not found exception.
     *
     * @param message the message
     */
    public NotFoundException(String message) {
        super(message);
    }

}
