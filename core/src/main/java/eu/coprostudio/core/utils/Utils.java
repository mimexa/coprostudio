package eu.coprostudio.core.utils;

import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Utils.
 */
public final class Utils {

    /**
     * Instantiates a new Util.
     */
    private Utils() {
    }

    /**
     * Dozer map list.
     *
     * @param <SOURCE>  the type parameter
     * @param <DEST>    the type parameter
     * @param source    the source
     * @param destClass the dest class
     * @return the list
     */
    public static <SOURCE, DEST> List<DEST> dozerMap(List<SOURCE> source, Class<DEST> destClass) {
        List<DEST> dest = new ArrayList<>();
        Mapper mapper = DozerBeanMapperBuilder.buildDefault();
        for (SOURCE item : source) {
            dest.add(mapper.map(item, destClass));
        }
        return dest;
    }

    /**
     * Dozer map dest.
     *
     * @param <SOURCE>  the type parameter
     * @param <DEST>    the type parameter
     * @param source    the source
     * @param destClass the dest class
     * @return the dest
     */
    public static <SOURCE, DEST> DEST dozerMap(SOURCE source, Class<DEST> destClass) {
        Mapper mapper = DozerBeanMapperBuilder.buildDefault();
        return mapper.map(source, destClass);
    }

}
